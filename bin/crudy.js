#! /usr/bin/env node

var fs     = require('fs');
var colors = require('colors');
var Create = require('../lib/Create');

console.log('=============================================================='.rainbow);
console.log('====      ==      ==  ==  ===================================='.rainbow);
console.log('====  ======  ==  ==  ==  ===================================='.rainbow);
console.log('====  ======  =  ===  ==  ===================================='.rainbow);
console.log('====  ======  = ====  ==  ===================================='.rainbow);
console.log('====  ======  ==  ==  ==  ===================================='.rainbow);
console.log('====      ==  ==  ==      ===================================='.rainbow);
console.log('=============================================================='.rainbow);

console.log('\n\n');

var args          = process.argv.slice(2);
var command       = 'CRUD'.split('');
var argumentsType = {
  full: new RegExp('--', 'gi'),
  light: new RegExp('-', 'gi'),
}
var options       = {
  target: 'vue',
  language: 'es6',
  api: 'http://localhost:4000/',
  output: null,
  input: process.env.PWD + '/models.json',
  home: process.env.HOME,
  currentDir: process.env.PWD
};



for (var i = 0; i < args.length; i++) {

  if (args[ i ] .match(/^(C|R|U|D)*$/gi)) {
    command = args[ i ];
  }
  if ( args[ i ].match(/^(-){2}/gi) || args[ i ].match(/^(-){1}/gi) ) {
    var arg = args[ i ].replace('--', '').replace('-', '');
    var argVal = args[ i + 1 ];
    switch( arg ) {
      case ('output' || 'o'):
        options.output   = process.env.PWD + '/' + argVal;
      break;
      case 'api':
        options.api      = arg;
      break;
      case 'language':
        options.language = argVal;
      break;
      case ('input' || 'i'):
        options.input    = process.env.PWD + '/' + argVal;
      break;
      default:
        console.warn('WARNING: "' + arg[ 0 ] + '" is not a valid argument.'.yellow);
    }
  }

}

if ( options.output === null ) {
  options.output = process.env.PWD + '/crudy/';
  console.warn('WARNING: No output folder. Writing to ' + options.output + ''.yellow);
} else {
  if (!fs.existsSync(options.output)) {
    console.warn('\nWARNING: ' + options.output + ' folder doesn\'t exist.'.yellow);
    console.log('Creating directory...'.green);
    fs.mkdirSync(options.output);
  }
}

var models = require(options.input);

for (var i = 0; i < command.length; i++) {
  switch (command[ i ].toLowerCase()) {
    case 'c':
      new Create(models, options);
      break;
    case 'r':
      // new Read();
      break;
    case 'u':
      // new Update();
      break;
    case 'd':
      // new Delete();
        break;
    default:
      console.warn('WARNING: "' + command[ i ] + '" is not a valid command. Please type crudy CRUD for full API or choose letters from "CRUD".'.yellow);
      console.warn('Type crudy --help to read the doc.'.yellow);
  }
}


console.log('\nWriting files in ' + options.output + ''.green);

console.log('\n\n');
