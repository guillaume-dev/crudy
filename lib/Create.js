'use strict';

let Templating = require('./Templating');

module.exports = class Create extends Templating {

  constructor(models, options) {

    super( models, options );
    this.type = 'create';

  }

  input( modelField, inputType, placeholder, required ) {
    return '\t\t<input name="' + modelField +
                '" type="' + inputType +
                '" placeholder="' + placeholder + '" '
                + required + '>\n';
  }

}
