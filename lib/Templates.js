'use strict';

let fs   = require('fs');
let path = require('path');


console.log("HEY", path.join(__dirname,'..', 'templates/vue/create.vue'));
module.exports = {

  vue: {
    create: fs.readFileSync(path.join(__dirname, '..', 'templates/vue/create.vue'), 'utf8', (err, data) => {return data;}),
    update: fs.readFileSync(path.join(__dirname, '..', 'templates/vue/update.vue'), 'utf8', (err, data) => {return data;}),
    delete: fs.readFileSync(path.join(__dirname, '..', 'templates/vue/delete.vue'), 'utf8', (err, data) => {return data;}),
    forms: {
      create: fs.readFileSync(path.join(__dirname, '..', 'templates/vue/forms/create.vue'), 'utf8', (err, data) => {return data;}),
      update: fs.readFileSync(path.join(__dirname, '..', 'templates/vue/forms/update.vue'), 'utf8', (err, data) => {return data;}),
      delete: fs.readFileSync(path.join(__dirname, '..', 'templates/vue/forms/delete.vue'), 'utf8', (err, data) => {return data;})
    }
  }

};
