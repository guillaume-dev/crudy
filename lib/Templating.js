'use strict';

let fs         = require('fs');
let templates  = require('./Templates');
let Handlebars = require('handlebars');

module.exports = class Templating {

  constructor(models, options) {

    this.content   = {};
    this.type      = 'create';
    this.extension = 'basic';
    this.options   = options;
    this.main      = templates[ options.target ][ this.type ];
    this.template  = Handlebars.compile( this.main );

    for (let modelKey in models) {

      if (models.hasOwnProperty(modelKey)) {

        let model = models[modelKey];

        this.content[ modelKey ] = '';

        for (let modelField in model) {
          if (model.hasOwnProperty(modelField)) {

            let fieldInfos = model[ modelField ];
            let required = fieldInfos.required ? "required" : "";

            if ( fieldInfos.type && fieldInfos.type === "String" ) {
              let inputType = fieldInfos.inputType ? fieldInfos.inputType : "text";
              let placeholder = fieldInfos.placeholder ? fieldInfos.placeholder : "";

              this.content[ modelKey ] += this.input( modelField, inputType, placeholder, required );
            }
          }
        }
      }
    }

    switch (this.options.target) {
      case 'vue':
        this.extension = 'vue';
      break;

    }

    for (let modelKey in models) {
      if (models.hasOwnProperty(modelKey)) {
        let form = this.form({
          id: modelKey,
          content: this.content[ modelKey ],
          apiUrl: this.options.api + modelKey
        });

        let type = this.type;

        console.log(type);
        fs.writeFile('../test/' + modelKey + type + '.' + this.extension, this.template({
          form: form
        }), function(err) {
          if(err) {
            return console.log(err)
          }

          console.log("The file was saved!");
        });

      }
    }

  }

  form(data) {
    var source = templates[ this.options.target ].forms[ this.type ];
    var template = Handlebars.compile(source);
    return template(data);
  }

  input( modelField, inputType, placeholder, required ) {
    return '\t\t<input name="' + modelField +
                '" type="' + inputType +
                '" placeholder="' + placeholder +
                '" value="<mustache-brackets> ' + modelField + ' </mustache-brackets>"'
                + required + '>\n';
  }
}
